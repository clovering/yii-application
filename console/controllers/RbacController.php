<?php
namespace console\controllers;

use Yii;
use yii\console\Controller;

class RbacController extends Controller
{
    /**
     * 注意 没有通过规则对权限验证，参见https://www.yiiframework.com/doc/guide/2.0/zh-cn/security-authorization
     *
     * @throws \yii\base\Exception
     */
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        // 添加 "createApk" 权限
        $createApk = $auth->createPermission('createApk');
        $createApk->description = '新增应用';
        $auth->add($createApk);

        // 添加 "updateApk" 权限
        $updateApk = $auth->createPermission('updateApk');
        $updateApk->description = '修改应用';
        $auth->add($updateApk);

        // 添加 "deleteApk" 权限
        $deleteApk = $auth->createPermission('deleteApk');
        $deleteApk->description = '删除应用';
        $auth->add($deleteApk);

        // 添加 "approveTimeli" 权限
        $approveTimeli = $auth->createPermission('approveTimeli');
        $approveTimeli->description = '审核版本';
        $auth->add($approveTimeli);


        // 添加 "apkAdmin" 角色并赋予 "updateApk" “deleteApk” “createApk”
        $apkAdmin = $auth->createRole('apkAdmin');
        $apkAdmin->description = '应用管理员';
        $auth->add($apkAdmin);
        $auth->addChild($apkAdmin, $updateApk);
        $auth->addChild($apkAdmin, $createApk);
        $auth->addChild($apkAdmin, $deleteApk);

        // 添加 "apkOperator" 角色并赋予  “deleteApk”
        $apkOperator = $auth->createRole('apkOperator');
        $apkOperator->description = '应用操作员';
        $auth->add($apkOperator);
        $auth->addChild($apkOperator, $deleteApk);

        // 添加 "timeliAuditor" 角色并赋予  “approveTimeli”
        $timeliAuditor = $auth->createRole('timeliAuditor');
        $timeliAuditor->description = '版本审核员';
        $auth->add($timeliAuditor);
        $auth->addChild($timeliAuditor, $approveTimeli);

        // 添加 "admin" 角色并赋予所有其他角色拥有的权限
        $admin = $auth->createRole('admin');
        $timeliAuditor->description = '系统管理员';
        $auth->add($admin);
        $auth->addChild($admin, $apkAdmin);
        $auth->addChild($admin, $timeliAuditor);



        // 为用户指派角色。其中 1 和 2 是由 IdentityInterface::getId() 返回的id （译者注：user表的id）
        // 通常在你的 User 模型中实现这个函数。
        $auth->assign($admin, 1);
        $auth->assign($apkAdmin, 2);
        $auth->assign($apkOperator, 3);
        $auth->assign($timeliAuditor, 4);
    }
}