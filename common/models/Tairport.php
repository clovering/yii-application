<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "t_airport".
 *
 * @property int $N_ID 主键
 * @property string $S_HEAD 头
 * @property string $S_NAME 名称
 * @property string $S_CREATEDATE
 */
class Tairport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_airport';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['S_CREATEDATE'], 'safe'],
            [['S_HEAD'], 'string', 'max' => 20],
            [['S_NAME'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'N_ID' => 'N  ID',
            'S_HEAD' => 'S  Head',
            'S_NAME' => 'S  Name',
            'S_CREATEDATE' => 'S  Createdate',
        ];
    }
}
