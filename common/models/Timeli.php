<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "timeli".
 *
 * @property int $id
 * @property string $content
 * @property int $status
 * @property int $create_time
 * @property int $userid
 * @property string $email
 * @property string $url
 * @property int $apk_id
 * @property int $remind 0:未提醒 1:已提醒
 */
class Timeli extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'timeli';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'status', 'userid', 'email', 'apk_id'], 'required'],
            [['content'], 'string'],
            [['status', 'create_time', 'userid', 'apk_id', 'remind'], 'integer'],
            [['email', 'url'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'content' => '内容',
            'status' => '状态',
            'create_time' => '创建时间',
            'userid' => '用户',
            'email' => 'Email',
            'url' => 'Url',
            'apk_id' => 'Apk ID',
            'remind' => '是否提醒',
        ];
    }

    /////////////////////---------------下面三个方法没有自动生成（手动补上去的），估计没有做好表之间的关联吧。---------

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApk()
    {
        return $this->hasOne(Apk::className(), ['id' => 'apk_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus0()
    {
        return $this->hasOne(Timelistatus::className(), ['id' => 'status']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'userid']);
    }

    public function getBeginning()
    {
        $tmpStr=strip_tags($this->content);
        $tmpLen=mb_strlen($tmpStr);

        return mb_substr($tmpStr,0,20,'utf-8').(($tmpLen>20)?'...':'');
    }

    public function approve()
    {
        $this->status=2;
        return ($this->save()?true:false);
    }

    public static function getPengdingTimeliCount()
    {
        return Timeli::find()->where(['status'=>1])->count();
    }

    public static function findRecentTimelis($limit=10)
    {
        return Timeli::find()->where(['status'=>2])->orderBy('create_time DESC')
            ->limit($limit)->all();
    }
}
