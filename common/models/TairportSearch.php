<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Tairport;

/**
 * TairportSearch represents the model behind the search form of `common\models\Tairport`.
 */
class TairportSearch extends Tairport
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['N_ID'], 'integer'],
            [['S_HEAD', 'S_NAME', 'S_CREATEDATE'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Tairport::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'N_ID' => $this->N_ID,
            'S_CREATEDATE' => $this->S_CREATEDATE,
        ]);

        $query->andFilterWhere(['like', 'S_HEAD', $this->S_HEAD])
            ->andFilterWhere(['like', 'S_NAME', $this->S_NAME]);

        return $dataProvider;
    }
}
