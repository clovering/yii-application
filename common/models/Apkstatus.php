<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "apkstatus".
 *
 * @property int $id
 * @property string $name
 * @property int $position
 *
 * @property Apk[] $apks
 */
class Apkstatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'apkstatus';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'position'], 'required'],
            [['position'], 'integer'],
            [['name'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'position' => 'Position',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApks()
    {
        return $this->hasMany(Apk::className(), ['status' => 'id']);
    }
}
