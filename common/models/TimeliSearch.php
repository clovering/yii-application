<?php

namespace common\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\Timeli;

/**
 * TimeliSearch represents the model behind the search form of `common\models\Timeli`.
 */
class TimeliSearch extends Timeli
{
    public function attributes()
    {
        return array_merge(parent::attributes(), ['user.username']);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'status', 'create_time', 'userid', 'apk_id'], 'integer'],
            [['content', 'email', 'url', 'user.username'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Timeli::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'timeli.id' => $this->id,
            'timeli.status' => $this->status,
            'create_time' => $this->create_time,
            'userid' => $this->userid,
            'apk_id' => $this->apk_id,
        ]);

        $query->andFilterWhere(['like', 'content', $this->content])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'url', $this->url]);
        //按照用户姓名搜索
        $query->join('INNER JOIN','user','timeli.userid = user.id');
        $query->andFilterWhere(['like','user.username',$this->getAttribute('user.username')]);
        //按照用户姓名排序
        $dataProvider->sort->attributes['user.username'] =
            [
                'asc'=>['user.username'=>SORT_ASC],
                'desc'=>['user.username'=>SORT_DESC],
            ];
        $dataProvider->sort->defaultOrder =
            [
                'status'=>SORT_ASC,
                'id'=>SORT_DESC,
            ];
        return $dataProvider;
    }
}
