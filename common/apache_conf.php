#1.vim /etc/apache2/httpd.conf
#2.打开LoadModule rewrite_module libexec/apache2/mod_rewrite.so
#3.在<DocumentRoot></DocumentRoot>节点内添加如下虚拟主机代码
#4.配置hosts
#5.如果虚拟主机apache conf文件不让修改，那么确保conf能被覆盖，并且在web根目录编写.htaccess
<VirtualHost *:80>
    ServerName yii-application.com
    DocumentRoot "D:/proj/php/phpstudy/phpStudy/PHPTutorial/WWW/yii-application/frontend/web/"

    <Directory "D:/proj/php/phpstudy/phpStudy/PHPTutorial/WWW/yii-application/frontend/web/">
    # use mod_rewrite for pretty URL support
    RewriteEngine on
    # If a directory or a file exists, use the request directly
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    # Otherwise forward the request to index.php
    RewriteRule . index.php

    # use index.php as index file
    DirectoryIndex index.php

    # ...other settings...
    </Directory>
</VirtualHost>

<VirtualHost *:80>
    ServerName admin.yii-application.com
    DocumentRoot "D:/proj/php/phpstudy/phpStudy/PHPTutorial/WWW/yii-application/backend/web/"

    <Directory "D:/proj/php/phpstudy/phpStudy/PHPTutorial/WWW/yii-application/backend/web/">
    # use mod_rewrite for pretty URL support
    RewriteEngine on
    # If a directory or a file exists, use the request directly
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    # Otherwise forward the request to index.php
    RewriteRule . index.php

    # use index.php as index file
    DirectoryIndex index.php

    # ...other settings...
    </Directory>
</VirtualHost>