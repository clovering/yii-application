<?php
namespace api\controllers;

use yii\rest\Controller;
use common\models\Apk;
use yii\db\Query;

/**
 * Class Top10Controller 不直接对应数据库表，所以不能从ActiveController继承
 * @package api\controllers
 */
class Top10Controller extends Controller
{
    public function actionIndex()
    {
        $top10 = (new Query())
            ->from('apk')
            ->select(['author_id','Count(id) as creatercount'])
            ->groupBy(['author_id'])
            ->orderBy('creatercount DESC')
            ->limit(10)
            ->all();

        return $top10;
    }

}