<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "t_sale_device_sensor".
 *
 * @property int $N_ID
 * @property string $S_DEVICE_NO 外部设备号
 * @property string $S_ADD_NAME 机场名
 * @property string $S_TELEPHONE 机场电话
 * @property string $S_SIGNAL_STRENGTH 信号强度ASU
 * @property int $N_NETWORK_ONLINE 贩售机网络是否在线（1：在线；0：不在线）
 * @property int $N_VMC_ONLINE 贩售机VMC是否在线（1：在线；0：不在线）
 * @property string $N_LONGITUDE 经度
 * @property string $N_LATITUDE 纬度
 * @property string $S_PROVIDER 提供者
 * @property string $S_ADDRESS 地址
 * @property string $S_CREATEDATE 定位时间
 */
class Devicesensor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 't_sale_device_sensor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['S_DEVICE_NO', 'N_LONGITUDE', 'N_LATITUDE'], 'required'],
            [['N_NETWORK_ONLINE', 'N_VMC_ONLINE'], 'integer'],
            [['N_LONGITUDE', 'N_LATITUDE'], 'number'],
            [['S_CREATEDATE'], 'safe'],
            [['S_DEVICE_NO', 'S_ADD_NAME', 'S_TELEPHONE', 'S_PROVIDER', 'S_ADDRESS'], 'string', 'max' => 255],
            [['S_SIGNAL_STRENGTH'], 'string', 'max' => 25],
            [['S_DEVICE_NO'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'N_ID' => 'N  ID',
            'S_DEVICE_NO' => 'S  Device  No',
            'S_ADD_NAME' => 'S  Add  Name',
            'S_TELEPHONE' => 'S  Telephone',
            'S_SIGNAL_STRENGTH' => 'S  Signal  Strength',
            'N_NETWORK_ONLINE' => 'N  Network  Online',
            'N_VMC_ONLINE' => 'N  Vmc  Online',
            'N_LONGITUDE' => 'N  Longitude',
            'N_LATITUDE' => 'N  Latitude',
            'S_PROVIDER' => 'S  Provider',
            'S_ADDRESS' => 'S  Address',
            'S_CREATEDATE' => 'S  Createdate',
        ];
    }
}
