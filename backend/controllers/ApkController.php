<?php

namespace backend\controllers;

use Yii;
use common\models\Apk;
use common\models\ApkSearch;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ApkController implements the CRUD actions for Apk model.
 */
class ApkController extends ACFController
{
    /**
     * {@inheritdoc}
     */
//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                'class' => VerbFilter::className(),
//                'actions' => [
//                    'delete' => ['POST'],
//                ],
//            ],
//        ];
//    }

    /**
     * Lists all Apk models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ApkSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Apk model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        // test code
//        $tmp = Apk::find()->where(['id'=>32])->one();
//        $tmp = Apk::findOne(32);
//        $tmp = Apk::find()->where(['status'=>2])->all();
//        $tmp = Apk::findAll(['status'=>2]);
//        $tmp = Apk::find()->where(['AND', ['status' => 2], ['id' => 32], ['LIKE', 'title', 'cloud']])->orderBy('id')->all();
//        var_dump($tmp);
//        foreach ($tmp as $item) {
//            echo $item->title;
//        }
//        exit(0);
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Apk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        if  (!Yii::$app->user->can('createApk')) {
            throw new ForbiddenHttpException('对不起，您没有新增应用权限！');
        }
        $model = new Apk();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Apk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        if  (!Yii::$app->user->can('updateApk')) {
            throw new ForbiddenHttpException('对不起，您没有修改应用权限！');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Apk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        if  (!Yii::$app->user->can('deleteApk')) {
            throw new ForbiddenHttpException('对不起，您没有删除应用权限！');
        }
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Apk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Apk::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }


}
