<?php
/**
 * Created by PhpStorm.
 * User: jiangchao
 * Date: 2018/8/29
 * Time: 19:45
 */

namespace backend\controllers;


use backend\models\Devicesensor;
use yii\web\Controller;

class VendorController extends ACFController
{

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionArea()
    {
        $model = new Devicesensor();
        $objects = Devicesensor::find()->select(['S_DEVICE_NO', 'S_ADD_NAME'])->all();
//        $dataProvider = json_encode($objects);
//        $dataProvider  = $model->findAll(null);
//        $dataProvider = \Yii::app()->db->createCommand('SELECT S_DEVICE_NO, S_ADD_NAME FROM t_sale_device_sensor')->queryAll();
        $array = (new \yii\db\Query())
            ->select('S_DEVICE_NO as deviceNo, S_ADD_NAME as addName, 
                S_TELEPHONE as telePhone, S_ADDRESS as address,
                    N_LONGITUDE as longitude, N_LATITUDE as latitude')
            ->from('t_sale_device_sensor')
            ->all();
        $dataProvider = json_encode($array);
        return $this->render('sensor', [
            'model' => $model,
            'dataProvider' => $dataProvider,
        ]);
    }

}