/**
 * Created by jiangchao on 2018/10/12.
 */
$(document).ready(function () {
    var map = new AMap.Map("container", {resizeEnable: true, center: [120.666921, 31.305551], zoom: 16});
    map.clearMap();
    var infoWindow;

    function infowindowdesc(no, address, telephone) {
        var template =
            "<img src='http://140.207.233.204:8181/goods/20170817174912.png' width='50' height='60'>"
            + address + "<br/><span>" + telephone +
            "</span><br/><a href='http://localhost:8080/admin/findAllItemChannelPage.html?deviceNo=" + no + "' target=‘_blank’>查看机器货道信息</a>";
        return template;
    }

    function mapFeatureClick(e) {
        if (!infoWindow) {
            infoWindow = new AMap.InfoWindow({
                isCustom: true,  //使用自定义窗体
                offset: new AMap.Pixel(16, -45)
            });
        }
        var extData = e.target.getExtData();
        var pos = new Array(extData.longitude, extData.latitude);
        infoWindow.setContent(createInfoWindow(extData.addName + "<span style='font-size:11px;color:#F00;'>" + extData.deviceNo + "</span>",
            infowindowdesc(extData.deviceNo, extData.address, extData.telePhone)));
        infoWindow.open(map, pos);
    }

    for (var data, i = 0, marker; i < dsList.length; i++) {
        data = dsList[i];
        marker = new AMap.Marker({
            position: new AMap.LngLat(data.longitude, data.latitude),
            map: map,
            extData: data
        });
        AMap.event.addListener(marker, 'click', mapFeatureClick);
    }

    //构建自定义信息窗体
    function createInfoWindow(title, content) {
        var info = document.createElement("div");
        info.className = "info";

        //可以通过下面的方式修改自定义窗体的宽高
        //info.style.width = "400px";
        // 定义顶部标题
        var top = document.createElement("div");
        var titleD = document.createElement("div");
        var closeX = document.createElement("img");
        top.className = "info-top";
        titleD.innerHTML = title;
        closeX.src = "https://webapi.amap.com/images/close2.gif";
        closeX.onclick = closeInfoWindow;

        top.appendChild(titleD);
        top.appendChild(closeX);
        info.appendChild(top);

        // 定义中部内容
        var middle = document.createElement("div");
        middle.className = "info-middle";
        middle.style.backgroundColor = 'white';
        middle.innerHTML = content;
        info.appendChild(middle);

        // 定义底部内容
        var bottom = document.createElement("div");
        bottom.className = "info-bottom";
        bottom.style.position = 'relative';
        bottom.style.top = '0px';
        bottom.style.margin = '0 auto';
        var sharp = document.createElement("img");
        sharp.src = "https://webapi.amap.com/images/sharp.png";
        bottom.appendChild(sharp);
        info.appendChild(bottom);
        return info;
    }

    //关闭信息窗体
    function closeInfoWindow() {
        map.clearInfoWindow();
    }

    map.on('complete', function(){
        map.plugin(["AMap.ToolBar", "AMap.OverView", "AMap.Scale"], function(){
            map.addControl(new AMap.ToolBar);
            map.addControl(new AMap.OverView({isOpen: true}));
            map.addControl(new AMap.Scale);
        });
    })
});