<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TimeliSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '更新历史';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timeli-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        <?//= Html::a('Create Timeli', ['create'], ['class' => 'btn btn-success']) ?>-->
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],

//            'id',
        [
                'attribute'=>'id',
                'contentOptions'=>['width'=>'30px'],
        ],
//            'content:ntext',
            [
                    'attribute'=>'content',
                    'value'=>'beginning',
//                    'value'=>function($model)
//             		{
//             			$tmpStr=strip_tags($model->content);
//             			$tmpLen=mb_strlen($tmpStr);
//
//             			return mb_substr($tmpStr,0,20,'utf-8').(($tmpLen>20)?'...':'');
//             		}
            ],
            [
                'attribute'=>'user.username',
                'label'=>'作者',
                'value'=>'user.username',
            ],
            //'status',
            [
                'attribute'=>'status',
                'value'=>'status0.name',
                'filter'=>\common\models\Timelistatus::find()
                    ->select(['name','id'])
                    ->orderBy('position')
                    ->indexBy('id')
                    ->column(),
                'contentOptions'=>
                    function($model)
                    {
                        return ($model->status==1)?['class'=>'bg-danger']:[];
                    }
            ],
//            'status',
            [
                'attribute'=>'create_time',
                'format'=>['date','php:m-d H:i'],
            ],
//            'userid',
            //'email:email',
            //'url:url',
            //'apk_id',
            'apk.title',
            ['class' => 'yii\grid\ActionColumn',
                'template'=>'{view} {update} {delete} {approve}',
                'buttons'=>
                    [
                        'approve'=>function($url,$model,$key)
                        {
                            $options=[
                                'title'=>Yii::t('yii', '审核'),
                                'aria-label'=>Yii::t('yii','审核'),
                                'data-confirm'=>Yii::t('yii','你确定通过这条版本记录吗？'),
                                'data-method'=>'post',
                                'data-pjax'=>'0',
                            ];
                            return Html::a('<span class="glyphicon glyphicon-check"></span>',$url,$options);
                        },
                    ],
//                'controller'=>'',
            ],
        ],
    ]); ?>
</div>
