<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Timeli */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Timelis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timeli-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'content:ntext',
            'status',
            'create_time:datetime',
            'userid',
            'email:email',
            'url:url',
            'apk_id',
        ],
    ]) ?>

</div>
