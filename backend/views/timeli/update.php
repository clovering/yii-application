<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Timeli */

$this->title = 'Update Timeli: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Timelis', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="timeli-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
