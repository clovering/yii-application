<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Timeli */

$this->title = 'Create Timeli';
$this->params['breadcrumbs'][] = ['label' => 'Timelis', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="timeli-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
