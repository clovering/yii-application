<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Tairport */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tairport-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'S_HEAD')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'S_NAME')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'S_CREATEDATE')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
