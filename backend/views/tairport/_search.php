<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\TairportSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tairport-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'N_ID') ?>

    <?= $form->field($model, 'S_HEAD') ?>

    <?= $form->field($model, 'S_NAME') ?>

    <?= $form->field($model, 'S_CREATEDATE') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
