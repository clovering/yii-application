<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Tairport */

$this->title = 'Create Tairport';
$this->params['breadcrumbs'][] = ['label' => 'Tairports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tairport-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
