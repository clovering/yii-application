<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TairportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tairports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tairport-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Tairport', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'N_ID',
            'S_HEAD',
            'S_NAME',
            'S_CREATEDATE',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
