<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Tairport */

$this->title = $model->N_ID;
$this->params['breadcrumbs'][] = ['label' => 'Tairports', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tairport-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->N_ID], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->N_ID], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'N_ID',
            'S_HEAD',
            'S_NAME',
            'S_CREATEDATE',
        ],
    ]) ?>

</div>
