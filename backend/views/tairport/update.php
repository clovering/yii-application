<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Tairport */

$this->title = 'Update Tairport: ' . $model->N_ID;
$this->params['breadcrumbs'][] = ['label' => 'Tairports', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->N_ID, 'url' => ['view', 'id' => $model->N_ID]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tairport-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
