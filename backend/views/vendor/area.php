<?php
/**
 * Created by PhpStorm.
 * User: jiangchao
 * Date: 2018/10/17
 * Time: 17:55
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TairportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = '贩售机分布图';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cache.amap.com/lbs/static/main1119.css"/>
<style type="text/css">
    .info {
        border: solid 1px silver;
    }
    div.info-top {
        position: relative;
        background: none repeat scroll 0 0 #F9F9F9;
        border-bottom: 1px solid #CCC;
        border-radius: 5px 5px 0 0;
    }
    div.info-top div {
        display: inline-block;
        color: #333333;
        font-size: 14px;
        font-weight: bold;
        line-height: 31px;
        padding: 0 10px;
    }
    div.info-top img {
        position: absolute;
        top: 10px;
        right: 10px;
        transition-duration: 0.25s;
    }
    div.info-top img:hover {
        box-shadow: 0px 0px 5px #000;
    }
    div.info-middle {
        font-size: 12px;
        padding: 6px;
        line-height: 20px;
    }
    div.info-bottom {
        height: 0px;
        width: 100%;
        clear: both;
        text-align: center;
    }
    div.info-bottom img {
        position: relative;
        z-index: 104;
    }
    span {
        margin-left: 5px;
        font-size: 11px;
    }
    .info-middle img {
        float: left;
        margin-right: 6px;
    }
    /*body { margin: 0; font: 13px/1.5 "Microsoft YaHei", "Helvetica Neue", "Sans-Serif"; min-height: 960px; min-width: 600px; }*/
    .vendor-area { margin: 0 auto; width: 600px; height: 600px; }
    .vendor-area .icon { background: url(http://lbs.amap.com/console/public/show/marker.png) no-repeat; }
    .vendor-area .icon-cir { height: 31px; width: 28px; }
    .vendor-area .icon-cir-red { background-position: -11px -5px; }
    .vendor-area .icon-cir-blue { background-position: -11px -55px; }
</style>
<div class="devicesensor-area">

    <h1><?= Html::encode($this->title) ?></h1>

<!--    <?//= var_dump($dataProvider);  ?>-->

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="pageBox span12" id="vendorArea">
                <div>
                    <div id="wrap" class="vendor-area">
                        <div id="container"></div>
                    </div>
                    <div id="tip">点击地图上的点标记，打开查看贩售机详细信息</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//webapi.amap.com/maps?v=1.3&key=8325164e247e15eea68b59e89200988b"></script>
<script type="text/javascript">
    var dsList = <?= $dataProvider;?>;
//    alert(dsList);
    var map = new AMap.Map("container", {resizeEnable: true, center: [120.666921, 31.305551], zoom: 16});
    map.clearMap();
    var infoWindow;

    function infowindowdesc(no, address, telephone) {
        var template =
            "<img src='http://140.207.233.204:8181/goods/20170817174912.png' width='50' height='60'>"
            + address + "<br/><span>" + telephone +
            "</span><br/><a href='http://localhost:8080/admin/findAllItemChannelPage.html?deviceNo=" + no + "' target=‘_blank’>查看机器货道信息</a>";
        return template;
    }

    function mapFeatureClick(e) {
        if (!infoWindow) {
            infoWindow = new AMap.InfoWindow({
                isCustom: true,  //使用自定义窗体
                offset: new AMap.Pixel(16, -45)
            });
        }
        var extData = e.target.getExtData();
        var pos = new Array(extData.longitude, extData.latitude);
        infoWindow.setContent(createInfoWindow(extData.addName + "<span style='font-size:11px;color:#F00;'>" + extData.deviceNo + "</span>",
            infowindowdesc(extData.deviceNo, extData.address, extData.telePhone)));
        infoWindow.open(map, pos);
    }

    for (var data, i = 0, marker; i < dsList.length; i++) {
        data = dsList[i];
        marker = new AMap.Marker({
            position: new AMap.LngLat(data.longitude, data.latitude),
            map: map,
            extData: data
        });
        AMap.event.addListener(marker, 'click', mapFeatureClick);
    }

    //构建自定义信息窗体
    function createInfoWindow(title, content) {
        var info = document.createElement("div");
        info.className = "info";

        //可以通过下面的方式修改自定义窗体的宽高
        //info.style.width = "400px";
        // 定义顶部标题
        var top = document.createElement("div");
        var titleD = document.createElement("div");
        var closeX = document.createElement("img");
        top.className = "info-top";
        titleD.innerHTML = title;
        closeX.src = "https://webapi.amap.com/images/close2.gif";
        closeX.onclick = closeInfoWindow;

        top.appendChild(titleD);
        top.appendChild(closeX);
        info.appendChild(top);

        // 定义中部内容
        var middle = document.createElement("div");
        middle.className = "info-middle";
        middle.style.backgroundColor = 'white';
        middle.innerHTML = content;
        info.appendChild(middle);

        // 定义底部内容
        var bottom = document.createElement("div");
        bottom.className = "info-bottom";
        bottom.style.position = 'relative';
        bottom.style.top = '0px';
        bottom.style.margin = '0 auto';
        var sharp = document.createElement("img");
        sharp.src = "https://webapi.amap.com/images/sharp.png";
        bottom.appendChild(sharp);
        info.appendChild(bottom);
        return info;
    }

    //关闭信息窗体
    function closeInfoWindow() {
        map.clearInfoWindow();
    }

    map.on('complete', function(){
        map.plugin(["AMap.ToolBar", "AMap.OverView", "AMap.Scale"], function(){
            map.addControl(new AMap.ToolBar);
            map.addControl(new AMap.OverView({isOpen: true}));
            map.addControl(new AMap.Scale);
        });
    })
</script>