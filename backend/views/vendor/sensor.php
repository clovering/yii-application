<?php
/**
 * Created by PhpStorm.
 * User: jiangchao
 * Date: 2018/10/17
 * Time: 17:55
 */
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\models\TairportSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->registerJsFile('@web/statics/js/sst.area.js', ['depends'=>'backend\assets\AppAsset']);
$this->registerJsFile('@web/statics/css/sst.area.css', ['depends'=>'backend\assets\AppAsset']);
$this->title = '贩售机分布图';
$this->params['breadcrumbs'][] = $this->title;
?>
<link rel="stylesheet" href="https://cache.amap.com/lbs/static/main1119.css"/>
<div class="devicesensor-area">

    <h1><?= Html::encode($this->title) ?></h1>

    <!--    <?//= var_dump($dataProvider);  ?>-->

    <div class="container-fluid">
        <div class="row-fluid">
            <div class="pageBox span12" id="vendorArea">
                <div>
                    <div id="wrap" class="vendor-area">
                        <div id="container"></div>
                    </div>
                    <div id="tip">点击地图上的点标记，打开查看贩售机详细信息</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//webapi.amap.com/maps?v=1.3&key=8325164e247e15eea68b59e89200988b"></script>
<script type="text/javascript" src="https://cache.amap.com/lbs/static/addToolbar.js"></script>
<script type="text/javascript" src="https://webapi.amap.com/demos/js/liteToolbar.js"></script>
<script type="text/javascript">
    var dsList = <?= $dataProvider;?>;
</script>