<?php
use common\models\Timeli;

$this->registerJsFile('@web/statics/css/vendor.css', ['depends'=>'backend\assets\AppAsset']);
$this->title = 'Vendor';
?>
<div class="container-fluid">
    <div class="quick-actions_homepage">
        <ul class="quick-actions">
            <li class="bg_lb span2">
                <a href="/admin/findPaymentByPage.html#card">
                    <i class="icon-list-alt"></i>支付管理</a>
            </li>
            <li class="bg_lo span2">
                <a href="/admin/findStaticSaleReportPage.html"><i class="icon-align-right"></i>卡片销售统计</a>
            </li>
            <li class="bg_lg span2">
                <a href="/admin/findAllItemSalePage.html"><i class="icon-th-list"></i>设备销售信息</a>
            </li>
            <li class="bg_db span2">
                <a href="/admin/staticSaleListPage.html"><i class="icon-align-right"></i>新增-卡片销售统计</a>
            </li>
            <li class="bg_lr span2">
                <a href="/admin/findAllItemSaleListPage.html"><i class="icon-th-list"></i>新增-销售详细信息</a>
            </li>
            <li class="bg_ls span2">
                <a href="/admin/findAllParseMessagePage.html"><i class="icon-indent-left"></i>Nayax解析信息</a>
            </li>
            <li class="bg_lg span2">
                <a href="/admin/originalRecordListPage.html"><i class="icon-indent-left"></i>Inhand解析信息</a>
            </li>
            <li class="bg_lo span2">
                <a href="/admin/woniuOrderListPage.html"><i class="icon-indent-left"></i>蜗牛设备解析信息</a>
            </li>
            <li class="bg_ls span2">
                <a href="/admin/deviceNoMappingListPage.html"><i class="icon-indent-left"></i>设备号映射</a>
            </li>
            <li class="bg_lo span2">
                <a href="/admin/goodsListPage.html"><i class="icon-hdd"></i>商品管理</a>
            </li>
            <li class="bg_ls span2">
                <a href="/admin/goodsCostListPage.html"><i class="icon-tasks"></i>商品成本管理</a>
            </li>
            <li class="bg_lo span2">
                <a href="/admin/cardListPage.html"><i class="icon-pencil"></i>商品名称映射</a>
            </li>
            <li class="bg_lg span2">
                <a href="/admin/deviceStoreRecordListPage.html"><i class="icon-pencil"></i>库存记录维护</a>
            </li>
            <li class="bg_ls span2">
                <a href="/admin/regionPage.html"><i class="icon-random"></i>地域信息维护</a>
            </li>
            <li class="bg_lg span2">
                <a href="/admin/goodsRegionPage.html"><i class="icon-retweet"></i>商品地域关联</a>
            </li>
            <li class="bg_lo span2">
                <a href="/admin/airportUserPage.html"><i class="icon-user"></i>系统用户维护</a>
            </li>
            <li class="bg_lg span2">
                <a href="/admin/vendlogs/page.html"><i class="icon-align-right"></i>设备日志</a>
            </li>
            <li class="bg_lg span2">
                <a href="/admin/messageConfigPage.html"><i class="icon-envelope"></i>短信人员</a>
            </li>
            <li class="bg_lg span2">
                <a href="/admin/messageAlarmPage.html"><i class="icon-envelope"></i>告警短信记录</a>
            </li>
            <li class="bg_ly span2">
                <a href="/index.php?r=tairport/index"><i class="icon-road"></i>机场配置</a>
            </li>

            <li class="bg_lr span2">
                <a href="/admin/sysDictPage.html"><i class="icon-desktop"></i>数据字典</a>
            </li>
            <li class="bg_ls span2">
                <a href="/admin/goodExpressPage.html"><i class="icon-book"></i>发货管理</a>
            </li>
            <li class="bg_ly span2">
                <a href="/admin/goodReceiverPage.html"><i class="icon-gift"></i>收货人管理</a>
            </li>
            <li class="bg_lv span2">
                <a href="/admin/sysRolePage.html"><i class="icon-bookmark"></i>角色管理</a>
            </li>
            <li class="bg_lo span2">
                <a href="/admin/goodStorePage.html"><i class="icon-shopping-cart"></i>机场库存管理</a>
            </li>
            <li class="bg_ls span2">
                <a href="<?=\yii\helpers\Url::to(['vendor/area']);?>"><i class="icon-shopping-cart"></i>贩售机分布</a>
            </li>
            <li class="bg_lb span2">
<!--                <a href="/index.php?r=apk/index"><i class="icon-shopping-cart"></i>应用管理</a>-->
                <a href="<?=Yii::$app->urlManager->createUrl(['apk/index']);?>"><i class="icon-shopping-cart"></i>应用管理</a>
            </li>
            <li class="bg_ly span2">
<!--                <a href="/index.php?r=timeli/index"><i class="icon-shopping-cart"></i>应用版本历史<?//='<span class="badge">'.Timeli::getPengdingTimeliCount().'</span>'?></a>-->
                <a href="<?=Yii::$app->urlManager->createUrl(['timeli/index']);?>"><i class="icon-shopping-cart"></i>应用版本历史<?='<span class="badge">'.Timeli::getPengdingTimeliCount().'</span>'?></a>
            </li>
            <li class="bg_lg span2">
<!--                <a href="/index.php?r=user/index"><i class="icon-retweet"></i>用户管理</a>-->
                <a href="<?=Yii::$app->urlManager->createUrl(['user/index']);?>"><i class="icon-retweet"></i>用户管理</a>
            </li>
        </ul>
    </div>
</div>
<!--End-pryRecord boxes-->
</div>

