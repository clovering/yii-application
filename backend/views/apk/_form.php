<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Apk */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="apk-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'tags')->textarea(['rows' => 6]) ?>

<!--   <?//= $form->field($model, 'status')->textInput() ?> -->
    <?php
        //第一种方法
        $psObjs = \common\models\Apkstatus::find()->all();
        $allStatus = \yii\helpers\ArrayHelper::map($psObjs,'id', 'name');
        var_dump($psObjs);
        echo "<br />--------------------------------------------------<br />";
        //第二种方法
        $psArray = Yii::$app->db->createCommand('select id, name from apkstatus')->queryAll();
        var_dump($psArray);
        $allStatus = \yii\helpers\ArrayHelper::map($psArray,'id', 'name');
        //第三种方法
        $allStatus = (new \yii\db\Query())
//            ->select(['id', 'name'])
            ->select(['name', 'id'])
            ->from('apkstatus')
            ->indexBy('id')
//            ->all();
            ->column();
        //第四种方法
        $allStatus =  \common\models\Apkstatus::find()
            ->select(['name', 'id'])
            ->orderBy('position')
            ->indexBy('id')
            ->column();

        $allStatusArray = (new \yii\db\Query())
        ->select(['name', 'id'])
        ->from('apkstatus')
        ->indexBy('id')
        ->all();//返回值元素 AR 数组
        $allStatusObj =  \common\models\Apkstatus::find()
        ->select(['name', 'id'])
        ->orderBy('position')
        ->indexBy('id')
        ->all();//返回值元素 AR 对象
        echo "<pre>";
        print_r($allStatusArray);
        echo "</pre>";
        echo "<pre>";
        print_r($allStatusObj);
        echo "</pre>";
    ?>

<!--   ><?//= $form->field($model, 'status')->dropDownList([1=>'草稿', 2=>'已发布'], ['prompt'=>'请选择状态']) ?>-->
    <?= $form->field($model, 'status')->dropDownList($allStatus, ['prompt'=>'请选择状态']) ?>
<!--    <?//= $form->field($model, 'create_time')->textInput() ?>-->
<!--    <?//= $form->field($model, 'update_time')->textInput() ?>-->

<!--    <?//= $form->field($model, 'author_id')->textInput() ?>-->
    <?= $form->field($model, 'author_id')->dropDownList(\common\models\Adminuser::find()
        ->select(['nickname', 'id'])
        ->indexBy('id')
        ->column(), ['prompt'=>'请选择作者']) ?>

    <div class="form-group">
        <?= Html::submitButton('保存', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
