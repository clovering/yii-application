<?php

namespace frontend\controllers;

use common\models\LoginForm;
use common\models\Tag;
use common\models\Timeli;
use common\models\User;
use frontend\models\ApkFrontSearch;
use Yii;
use common\models\Apk;
use common\models\ApkSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ApkController implements the CRUD actions for Apk model.
 */
class ApkController extends Controller
{
    public $added=0; //0代表还没有新Timeli
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            //页面缓存使用的同时还可以使用片段缓存、数据缓存以及renderDynamic动态数据
            'pageCache'=>[
                'class'=>'yii\filters\PageCache',
                'only'=>['index'],
                'duration'=>600,
                'variations'=>[//可以根据下面两个参数设置不同缓存,page和ApkFrontSearch所处位置在url中，他们两个是引起变化的因素
                    Yii::$app->request->get('page'),
                    Yii::$app->request->get('ApkFrontSearch'),
                ],
                'dependency'=>[
                    'class'=>'yii\caching\DbDependency',
                    'sql'=>'select count(id) from apk',
                ],
            ],
            'httpCache'=>[
                'class'=>'yii\filters\HttpCache',
                'only'=>['detail'],
                'lastModified'=>function ($action,$params){
                    $q = new \yii\db\Query();
                    return $q->from('apk')->max('update_time');//根据apk的最后修改时间来做http缓存的Last Modified，可以在chrome开发者工具中查看
                },
                'etagSeed'=>function ($action,$params) {
                    $post = $this->findModel(Yii::$app->request->get('id'));
                    return serialize([$post->title,$post->content]);//可以在chrome开发者工具中查看etag
                },

                'cacheControlHeader' => 'public,max-age=600',//可以在chrome开发者工具中查看Cache Control

            ],
        ];
    }

    /**
     * Lists all Apk models.
     * @return mixed
     */
    public function actionIndex()
    {
        $tags = Tag::findTagWeights();
        $recentTimelis = Timeli::findRecentTimelis();
        //注意，如果其他地方都用ApkFrontSearch，这里传递到页面的也要用ApkFrontSearch
        $searchModel = new ApkFrontSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'tags' => $tags,
            'recentTimelis' => $recentTimelis,
        ]);
    }

    /**
     * Displays a single Apk model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Apk model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Apk();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Apk model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Apk model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Apk model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Apk the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Apk::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

    public function actionDetail($id)
    {
        //step1
        $model = $this->findModel($id);
        $tags = Tag::findTagWeights();
        $recentTimelis = Timeli::findRecentTimelis();

        $userMe = User::findOne(Yii::$app->user->id);

        if (!$userMe) {
            return $this->redirect(['site/login',
                'model' => new LoginForm(),//去登录吧
            ]);
        }

        $timeliModel = new Timeli();
        $timeliModel->email = $userMe->email;
        $timeliModel->userid = $userMe->id;

        //step2
        if($timeliModel->load(Yii::$app->request->post()))
        {
            $timeliModel->status = 1; //新评论默认状态为 pending
            $timeliModel->apk_id = $id;
            if($timeliModel->save())
            {
                $this->added=1;
            }
        }

        //step3.传数据给视图渲染

        return $this->render('detail',[
            'model'=>$model,
            'tags'=>$tags,
            'recentTimelis'=>$recentTimelis,
            'timeliModel'=>$timeliModel,
            'added'=>$this->added,
        ]);
    }
}
