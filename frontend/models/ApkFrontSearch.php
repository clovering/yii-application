<?php
/**
 * Created by PhpStorm.
 * User: jiang
 * Date: 2018/10/8
 * Time: 22:30
 */

namespace frontend\models;


use common\models\ApkSearch;

/**
 * Class ApkFrontSearch
 * 本项目前后台ApkFrontSearch和ApkBackendSearch都一样，都可以直接用ApkSearch
 * @package frontend\models
 */
class ApkFrontSearch extends ApkSearch
{

}