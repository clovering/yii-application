<?php
namespace frontend\components;

use yii\base\Widget;
use yii\helpers\Html;

class RctReplyWidget extends Widget
{
    public $recentTimelis;

    public function init()
    {
        parent::init();
    }

    public function run()
    {
        $timeliString='';

        foreach ($this->recentTimelis as $timeli)
        {
            $timeliString.='<div class="apk">'.
                '<div class="title">'.
                '<p style="color:#777777;font-style:italic;">'.
                nl2br($timeli->content).'</p>'.
                '<p class="text"> <span class="glyphicon glyphicon-user" aria-hidden="ture">
							</span> '.Html::encode($timeli->user->username).'</p>'.

                '<p style="font-size:8pt;color:bule">
							《<a href="'.$timeli->apk->url.'">'.Html::encode($timeli->apk->title).'</a>》</p>'.
                '<hr></div></div>';
        }
        return  $timeliString;
    }
}