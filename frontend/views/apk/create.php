<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\models\Apk */

$this->title = 'Create Apk';
$this->params['breadcrumbs'][] = ['label' => 'Apks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="apk-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
