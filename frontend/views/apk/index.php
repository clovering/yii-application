<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel common\models\ApkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

?>
<div class="container">
    <div class="row">
        <div class="col-md-9">
            <ol class="breadcrumb">
                <li><a href="<?= Yii::$app->homeUrl;?>">首页</a></li>
                <li>应用列表</li>

            </ol>
            <?=
                \yii\widgets\ListView::widget([
                        'id'=>'postList',
                        'dataProvider'=>$dataProvider,
                        'itemView'=>'_listitem',//单元格子试图
                        'layout'=>'{items} {pager}',
                        'pager'=>[
                            'maxButtonCount'=>10,
                            'nextPageLabel'=>Yii::t('app','下一页'),
                            'prevPageLabel'=>Yii::t('app','上一页'),
                            ],
                ]);
            ?>
        </div>
        <div class="col-md-3">
            <div class="searchbox">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="glyphicon glyphicon-search"></span>查找应用
                        <?php
                            //数据缓存示例代码
                            $data = Yii::$app->cache->get('apkCount');
                            $dependency = new \yii\caching\DbDependency(['sql'=>'select count(id) from apk']);
                            if ($data === false)
                            {
                                $data = \common\models\Apk::find()->count();
                                //sleep(5);
                                Yii::$app->cache->set('apkCount', $data, 600, $dependency); //设置缓存60秒后过期
                            }

                            echo $data;
                        ?>
                    </li>
                    <li class="list-group-item">
                        <form>
                            <div class="form-group" action="index.php?r=apk/index" id="w0" method="get">
                                <input type="text" class="form-control" name="ApkFrontSearch[title]" id="w0input" placeholder="按标题">
                            </div>
                            <button type="submit" class="btn btn-default">搜索</button>
                        </form>
                    </li>
                </ul>
            </div>
            <div class="tagcloudbox">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="glyphicon glyphicon-search"></span>标签云
                    </li>
                    <li class="list-group-item">
                        //片段缓存示例代码 里面如果有小段动态内容可以用renderDynamic，还支持嵌套
                        /*
                        $dependency = new \yii\caching\DbDependency(['sql'=>'select count(id) from apk']);

                        if ($this->beginCache('cache',['duration'=>600],['dependency'=>$dependency]))
                        {
                        echo TagsCloudWidget::widget(['tags'=>$tags]);
                        $this->endCache();
                        }
                        */
                        <?= \frontend\components\TagsCloudWidget::widget(['tags'=>$tags]);?>
                    </li>
                </ul>
            </div>
            <div class="commentbox">
                <ul class="list-group">
                    <li class="list-group-item">
                        <span class="glyphicon glyphicon-search"></span>最近的版本记录
                    </li>
                    <li class="list-group-item">
                        <?= \frontend\components\RctReplyWidget::widget(['recentTimelis'=>$recentTimelis]);?>
                    </li>
                </ul>
            </div>
        </div>
    </div>

</div>
