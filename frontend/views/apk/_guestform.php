<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\timeli */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="timeli-form">

    <?php $form = ActiveForm::begin([
        'action'=>['apk/detail','id'=>$id,'#'=>'timelis'],
        'method'=>'post',
    ]); ?>


    <div class="row">
        <div class="col-md-12"><?= $form->field($timeliModel,'content')->textarea(['row'=>6])?></div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('发表版本记录', ['class' =>'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>