<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<?php foreach($timelis as $timeli): ?>

    <div class="timeli">

        <div class="row">
            <div class="col-md-12">
                <div class="timeli_detail">
                    <p class="bg-info">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                        <em><?= Html::encode($timeli->user->username);?>;</em>
                        <br>
                        <?= nl2br($timeli->content);?>
                        <br>
                        <span class="glyphicon glyphicon-time" aria-hidden="true"></span>
                        <em><?= date('Y-m-d H:i:s',$timeli->create_time);?>;</em>
                    </p>
                </div>
            </div>
        </div>
    </div>

<?php endforeach;?>